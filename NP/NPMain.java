package NP;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class NPMain extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Nicolas Nobili - CSCI 13 Final Project");

        showNPView();
    }

    public void showNPView() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(NPMain.class.getResource("NPView.fxml"));
            
            AnchorPane NPView = (AnchorPane) loader.load();
            

            // Show the scene containing the root layout.
            Scene scene = new Scene(NPView);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
